<div id="footer_html">
  <div class="footer-followme">
    <div class="inner">
      <div class="footer-feedback">
        <div class="body">
          <div class="title">
            Votre avis nous intéresse
          </div>
          <div class="content">
            Cette page a-t-elle répondu à <br>
            votre attente?
          </div>
        </div>
        <div class="actions">
          <button>Oui</button>
          <button>Non</button>
        </div>
      </div>
      <div class="footer-newsletter">

      </div>
    </div>
  </div>
  <div class="footer-socialmedia">
    <div class="inner">
      <div class="title">
        Suivez nous<br>
        également sur
      </div>
      <ul>
        <li>
          <a href="#" class="mob-icon-socialmedia-twitter">Twitter</a>
        </li>
        <li>
          <a href="#" class="mob-icon-socialmedia-facebook">Facebook</a>
        </li>
        <li>
          <a href="#" class="mob-icon-socialmedia-youtube">Youtube</a>
        </li>
        <li>
          <a href="#" class="mob-icon-socialmedia-google">Google</a>
        </li>
        <li>
          <a href="#" class="mob-icon-socialmedia-instagram">Instagram</a>
        </li>
        <li>
          <a href="#" class="mob-icon-socialmedia-foursquare">Foursquare</a>
        </li>
        <li>
          <a href="#" class="mob-icon-socialmedia-linkedin">LinkedIn</a>
        </li>
      </ul>
    </div>
  </div>
  <div class="footer-links">
    <div class="inner">
      <ul class="column">
        <li class="title"><a href="/nl" title="">Mobistar</a></li>
        <li><a href="/nl/contact" title="Contacteer ons">Contacteer ons</a></li>
        <li><a href="/shoplocator" title="Onze Mobistar Center">Onze Mobistar Center</a></li>
        <li><a href="/nl/hulp/netwerk " title="Ons netwerk">Ons netwerk</a></li>
        <li><a href="/nl/aanbod/mobiel-internet/internet-op-gsm/4g-van-mobistar" title="4G">4G</a></li>
        <li><a href="http://business.mobistar.be/nl/" title="Business">Professionals en Bedrijven</a></li>
        <li><a href="http://corporate.mobistar.be/nl/index.cfm" title="">Het bedrijf Mobistar</a></li>
        <li><a href="http://corporate.mobistar.be/go/nl/Onze_mensen.cfm" title="Vacatures">Vacatures</a></li>
      </ul>
      <ul class="column">
        <li class="title"><a href="/klantenzone" title="">Klantenzone</a></li>
        <li><a href="/registreren" title="">Registreren</a></li>
        <li><a href="/herladen" title="">Kaart Herladen</a></li>
        <li><a href="https://e-services.mobistar.be/nl/invoices" title="Mijn facturen">Mijn facturen</a></li>
        <li><a href="https://e-services.mobistar.be/nl/invoices/your-usage/" title="Mijn verbruik controleren">Verbruik controleren</a></li>
        <li><a href="https://e-services.mobistar.be/nl/tariff-change/" title="Tariefplan wijzigen">Wijzig je tariefplan</a></li>
        <li><a href="https://e-services.mobistar.be/nl/subscriptions/options-and-services/" title="Opties &amp; diensten beheren">Opties beheren</a>
        </li></ul>
      <ul class="column">
        <li class="title"><a href="/nl/hulp" title="Hulp">Hulp</a></li>
        <li><a href="/nl/hulp" title="Veelgestelde vragen">Veelgestelde vragen</a></li>
        <li><a href="/nl/aanbod/mnp" title="Behoud je nummer">Behoud je nummer</a></li>
        <li><a href="/nl/hulp/je-toestel/je-toestel-herstellen" title="Reparatie">Reparatie</a></li>
        <li><a href="/nl/hulp/mobiele-telefonie/je-voicemail/hoe-kan-ik-op-de-juiste-manier-mijn-gsm-configureren" title="Gsm-configuratie">Gsm-configuratie</a></li>
        <li><a href="/nl/hulp/mobiele-telefonie/pin-puk-en-imei-codes/ik-ben-mijn-pincode-vergeten" title="Pincode vergeten">Pincode vergeten</a></li>
      </ul>
      <ul class="column">
        <li class="title margin-bottom-23"><a href="/nl/aanbod/opties-en-diensten" title="Opties en diensten">Opties<br>&amp; diensten</a></li>
        <li><a href="/nl/hulp/mobiele-telefonie/je-factuur/ik-wil-graag-een-webfactuur-ontvangen" title="">Webfactuur</a></li>
        <li><a href="/nl/aanbod/opties-en-diensten/mobiele-telefonie/controleren" title="">Budgetcontrole</a></li>
        <li><a href="/nl/aanbod/opties-en-diensten/mobiele-telefonie/gsm-verbruik-opvolgen" title="">Verbruiksinfo</a></li>
        <li><a href="/nl/aanbod/opties-en-diensten/mobiele-telefonie/herladen" title="">Herlaaddiensten</a></li>
        <li><a href="/nl/aanbod/opties-en-diensten/gsm-verzekering" title="">Verzekering</a></li>
        <li><a href="/nl/aanbod/opties-en-diensten/cloud" title="">Mobile Cloud</a></li>
      </ul>
      <ul class="column">
        <li class="title margin-bottom-23">Producten<br>&amp; diensten</li>
        <li><a href="/nl/aanbod/mobiele-telefonie" title="Mobiele telefonie">Mobiele telefonie</a></li>
        <li><a href="/nl/aanbod/herlaadkaarten" title="Herlaadkaarten">Herlaadkaarten</a></li>
        <li><a href="/nl/aanbod/herlaadkaarten/gratis-simkaart" title="Gratis simkaart">Gratis simkaart</a></li>
        <li><a href="/nl/aanbod/gsm-abonnementen" title="Gsm-abonnementen">Gsm-abonnementen</a></li>
        <li><a href="http://business.mobistar.be/nl/professionals/business-oplossingen/mobiele-telefonie/abonnementen/" title="Pro-abonnementen (voor zelfstandigen)">Gsm-abonnementen voor zelfstandigen</a></li>
        <li><a href="/nl/aanbod/mobiel-internet" title="Mobiel internet">Mobiel internet</a></li>
        <li><a href="/nl/aanbod/info/bellen-in-het-buitenland" title="Bellen in het buitenland (Roaming)">Roaming</a></li>
        <li><a href="/nl/aanbod/mobistar-voor-iedereen" title="Hulp voor personen met een handicap">Hulp voor personen met een handicap</a></li>
      </ul>
      <ul class="column">
        <li class="title margin-bottom-23"><a href="/nl/aanbod/shop/smartphone" title="Top gsm &amp; smartphones">Top gsm &amp; smartphones</a></li>
        <li><a href="/nl/aanbod/info/iphone-5">iPhone 5S</a></li>
        <li><a href="/nl/aanbod/info/iphone-4s">iPhone 4S</a></li>
        <li><a href="/nl/aanbod/info/samsung-galaxy-s4">Samsung Galaxy S4</a></li>
        <li><a href="/nl/aanbod/shop/smartphone/samsung-galaxy-siii-titanium-grijs-16gb">Samsung Galaxy S3</a></li>
        <li><a href="/nl/aanbod/shop/smartphone/samsung-galaxy-siii-mini">Samsung Galaxy S3 Mini</a></li>
        <li><a href="/nl/aanbod/shop/smartphone/nokia-lumia-625-black">Nokia Lumia 625</a></li>
        <li><a href="/nl/aanbod/shop/smartphone/sony-xperia-sp-black">Sony Xperia SP</a></li>
      </ul>


    </div>

  </div>
  <div class="footer-bottom">
    <div class="inner">
      <ul>
        <li><a href="/algemenevoorwaarden" title="Algemene voorwaarden">Algemene voorwaarden</a></li>
        <li><a href="/nl/privacypolicy" title="Privacy Policy">Privacy Policy</a></li>
        <li><a href="/consumenteninlichtingen" title="Consumenteninlichtingen">Consumenteninlichtingen</a></li>
        <li><a href="/nl/siteconditions" title="Gebruiksvoorwaarden">Gebruiksvoorwaarden</a></li>
        <li><a href="/nl/companyprofile" title="">Bedrijfsgegevens</a></li>
        <li class="copyright">© Mobistar <script type="text/javascript">;document.write(new Date().getFullYear());</script></li>
      </ul>
    </div>
  </div>
</div>

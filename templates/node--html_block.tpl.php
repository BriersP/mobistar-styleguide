<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php if ($title): ?>
  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <div class="row top">
      <?php foreach ($top as $key => $content) : ?>
        <div class ="col top-<?php print $key + 1; ?> <?php print $content['classes']; ?>">
          <?php print render($content['content']); ?>
        </div>
      <?php endforeach; ?>
    </div>
    <div class="row middle">
      <?php foreach ($middle as $key => $content) : ?>
        <div class ="col middle-<?php print $key + 1; ?> <?php print $content['classes']; ?>">
          <?php print render($content['content']); ?>
        </div>
      <?php endforeach; ?>
    </div>
    <div class="row bottom">
      <?php foreach ($bottom as $key => $content) : ?>
        <div class ="col bottom-<?php print $key + 1; ?> <?php print $content['classes']; ?>">
          <?php print render($content['content']); ?>
        </div>
      <?php endforeach; ?>
    </div>
  </div>

</div>

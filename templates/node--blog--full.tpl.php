<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
    <?php print render($content['field_blog_category']); ?>
    <div class="feedback">
      <div class="comments<?php print ($comment_count > 0 ) ? ' active' : ''; ?>"><span><?php print $comment_count ?></span></div>
    </div>
    <?php print render($title_prefix); ?>
    <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
    <?php print render($title_suffix); ?>
    <div class="rating"><?php print render($content['blog_rating']); ?></div>
    <?php print render($content['field_blog_author']); ?>
    <?php print render($content['created_formatted']); ?>
    <?php print render($content['field_blog_featured_image']); ?>
    <?php print render($content['body']); ?>
    <?php print render($content['blog_author_profile']); ?>
    <?php print render($content['field_blog_review']); ?>
    <?php print render($content['comments']); ?>
  </div>

</div>

<?php
/**
 * @file
 * The template file of the module
 *
 * Variables used:
 *  - $links: array of links in each language enabled.
 */
$i = 1;
$languages = language_list();
$query = drupal_get_query_parameters();
$from = array_key_exists('destination', $query) ? $query['destination'] : '<front>';
?>

<div class="language_selection_page_body">
  <div class="language_selection_page_body_inner">

    <img class="logo" src="/<?php print drupal_get_path('theme', 'mobistarb2c'); ?>/images/logo-splash.png">

    <?php foreach ($links['items'] as $language_code => $markup): ?>
      <?php $class = ($i % 2) ? 'odd' : 'even'; ?>
      <div class="language <?php print $language_code; ?> <?php print $class; ?>">
        <div class="language-content">
          <h2><?php print $languages[$language_code]->native; ?></h2>
          <?php
          $customer_link_text = i18n_variable_get('customer_link_text', $language_code, 'For customers');
          print l($customer_link_text, $from, array('attributes' => array('class' => array('language-link', 'mob-button', 'mob-white')), 'language' => $languages[$language_code]));
          $business_link_text = i18n_variable_get('business_link_text', $language_code, 'Professionals and business');
          print l($business_link_text, 'http://business.mobistar.be', array('attributes' => array('class' => array('language-link', 'mob-button', 'mob-white')), 'language' => $languages[$language_code]));
          ?>
        </div>
      </div>
      <?php $i++; ?>
    <?php endforeach; ?>
    <?php $i = 1; ?>
    <?php foreach ($links['items'] as $language_code => $markup): ?>
      <?php $class = ($i % 2) ? 'odd' : 'even'; ?>
      <div class="about <?php print $language_code; ?> <?php print $class; ?>">
        <h2><?php print i18n_variable_get('splash_intro_title', $language_code, ''); ?></h2>
        <div class="about-content">
          <?php
          $body = i18n_variable_get('splash_intro_body', $language_code, array('value' => '', 'format' => 'filtered_html'));
          print check_markup($body['value'], $body['format']);
          ?>
        </div>
      </div>
      <?php $i++; ?>
    <?php endforeach; ?>
  </div>
</div>

<div class="language_selection_page_footer">
  <div class="language_selection_page_footer_inner">
    <div class="content">
      <?php
      $footer_content = variable_get('splash_footer_content', array('value' => '', 'format' => 'filtered_html'));
      print check_markup($footer_content['value'], $footer_content['format']);
      ?>
    </div>
  </div>
</div>

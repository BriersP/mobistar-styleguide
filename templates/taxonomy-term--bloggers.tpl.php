<?php
/**
 * @file
 * Theme implementation to display a bloggers term in teaser mode.
 */
?>
<div id="taxonomy-term-<?php print $term->tid; ?>" class="<?php print $classes; ?>">
  <div class="author">
    <?php print render($content['field_blogger_picture']); ?>
    <?php print render($content['field_blogger_first_name']); ?> <span class="company">@ Mobistar</span>
  </div>
  <div class="body">
    <?php print render($content['description']); ?>
  </div>
</div>

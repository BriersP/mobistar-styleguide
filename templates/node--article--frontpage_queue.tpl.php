<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix widget-article-preview"<?php print $attributes; ?>>

  <div class="image">
    <?php print render($content['field_article_header_image']); ?>
  </div>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <div class="body" <?php print $content_attributes; ?>>
    <?php print render($content['body']); ?>
  </div>

  <div class="more">
    <a ref="<?php print $node_url; ?>"><?php print t('Read more'); ?></a>
  </div>

</div>

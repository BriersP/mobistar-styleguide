<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <!-- following element is for indication only.
  Feel free to remove it, when theming is done. -->
  <h2>FEATURED ITEM</h2>
  <?php print render($content['field_blog_featured_image']); ?>
  <div class="content"<?php print $content_attributes; ?>>
    <?php print render($title_prefix); ?>
    <?php if (!$page): ?>
      <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php print render($content['field_blog_author']); ?>
    <?php print render($content['created_formatted']); ?>
    <?php print render($content['body']); ?>
    <a class="more" href="<?php print $node_url; ?>"><?php print t('Read more'); ?></a>
  </div>
  <div class="feedback">
    <div class="rating"><?php print render($content['blog_rating']); ?></div>
    <div class="comments<?php print ($comment_count > 0 ) ? ' active' : ''; ?>">
      <span><?php print $comment_count ?></span>
    </div>
  </div>
</div>

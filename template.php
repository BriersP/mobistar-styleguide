<?php

/**
 * Adding the title of the current page to the breadcrumb.
 */
function mobistarb2c_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  if (!empty($breadcrumb)) {
    $breadcrumb[] = drupal_get_title();
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
    $output .= '<div class="breadcrumb">' . implode(' <span class="beforetitle">»</span> ', $breadcrumb) . '</div>';
    return $output;
  }
}

/**
 * Implements hook_preprocess_node().
 */
function mobistarb2c_preprocess_node(&$vars) {
  // Make "node--NODETYPE--VIEWMODE.tpl.php" templates available for nodes
  $vars['theme_hook_suggestions'][] = 'node__' . $vars['type'] . '__' . $vars['view_mode'];

  // Merge extra classes from content on the wrapper.
  // Used by style plugins to add classes to node wrapper.
  if (isset($vars['content']['extra_classes_node'])) {
    $vars['classes_array'] = array_merge($vars['classes_array'], $vars['content']['extra_classes_node']);
    unset($vars['content']['extra_classes_node']);
  }
}

/**
 * Implements preprocess_page.
 */
function mobistarb2c_preprocess_page(&$variables) {
  $variables['footer_html'] = theme('footer_html');
  $variables['header_html'] = theme('header_html');
//  $block['footer']['#markup'] = theme('footer', $arg);
}

/**
 * Implements hook_theme().
 */
function mobistarb2c_theme() {
  $theme['footer_html'] = array(
    'path' => drupal_get_path('theme', 'mobistarb2c') . '/templates',
    'template' => 'footer'
  );
  $theme['header_html'] = array(
    'path' => drupal_get_path('theme', 'mobistarb2c') . '/templates',
    'template' => 'header'
  );
  return $theme;
}

function mobistarb2c_preprocess_panels_pane(&$vars) {
  // Merge extra classes from content on the wrapper.
  // Used by style plugins to add classes to pane wrapper.
  if (!empty($vars['content']['extra_classes_pane']) && is_array($vars['content']['extra_classes_pane'])) {
    $vars['classes_array'] = array_merge($vars['classes_array'], $vars['content']['extra_classes_pane']);
  }
  else {
    $vars['classes_array'][] = 'default';
  }
}

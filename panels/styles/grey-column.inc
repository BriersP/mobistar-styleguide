<?php

/**
 * @file
 * Definition of the 'black' panel style.
 */
// Plugin definition
$plugin = array(
  'title' => t('Grey column'),
  'description' => t('Display the columns of the pane on with a black background.'),
  'render pane' => 'mobistarb2c_grey_column_style_render_pane',
  'weight' => -5,
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_mobistarb2c_grey_column_style_render_pane($vars) {
  $content = $vars['content'];
  $pane = $vars['pane'];
  $display = $vars['display'];

  if (empty($content->content)) {
    return;
  }
  // @see mobistarb2c_preprocess_panels_pane()
  $content->content['extra_classes_pane'][] = 'grey-column';
  $out = theme('panels_pane', array('content' => $content, 'pane' => $pane, 'display' => $display));
  return $out;
}
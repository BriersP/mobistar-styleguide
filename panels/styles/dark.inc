<?php

/**
 * @file
 * Definition of the 'black' panel style.
 */
// Plugin definition
$plugin = array(
  'title' => t('Dark background'),
  'description' => t('Display the pane on a dark background.'),
  'render pane' => 'mobistarb2c_dark_style_render_pane',
  'weight' => -5,
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_mobistarb2c_dark_style_render_pane($vars) {
  $content = $vars['content'];
  $pane = $vars['pane'];
  $display = $vars['display'];

  if (empty($content->content)) {
    return;
  }
  $pane->css['css_class'] .= ' ';
  $pane->css['css_class'] .= 'dark-background';
  // @see mobistarb2c_preprocess_panels_pane()
  $content->content['extra_classes_pane'][] = 'dark-background';
  $out = theme('panels_pane', array('content' => $content, 'pane' => $pane, 'display' => $display));
  return $out;
}

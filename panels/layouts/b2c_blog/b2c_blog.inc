<?php

// Plugin definition
$plugin = array(
  'title' => t('Blog overview'),
  'category' => t('Mobistar b2c'),
  'icon' => 'b2c_blog.png',
  'theme' => 'panels_b2c-blog',
  'regions' => array(
    'blog-top' => t('Top'),
    'blog-left' => t('Left'),
    'blog-right' => t('Right'),
  ),
);

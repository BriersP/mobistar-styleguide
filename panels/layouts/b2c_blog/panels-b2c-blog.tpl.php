<div class="blog-content">
  <div id="blog-top">
    <?php print $content['blog-top']; ?>
  </div>
  <div id="blog-content-wrapper">
    <div id="blog-left" class="grid-8 first"><?php print $content['blog-left']; ?></div>
    <div id="blog-right" class="grid-4 last">
      <?php if(function_exists('get_subscribe_links')): ?>
        <?php print get_subscribe_links(); ?>
      <?php endif; ?>
      <?php print $content['blog-right']; ?>
    </div>
  </div>
</div>


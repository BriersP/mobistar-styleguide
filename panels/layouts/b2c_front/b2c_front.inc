<?php

// Plugin definition
$plugin = array(
  'title' => t('Frontpage'),
  'category' => t('Mobistar b2c'),
  'icon' => 'b2c_front.png',
  'theme' => 'panels_b2c-front',
  'regions' => array(
    'tabs-wrapper' => t('Tabs'),
    'tab-products' => t('Products and services'),
    'tab-store' => t('Store'),
    'tab-support' => t('Support'),
    'tab-customerzone' => t('Customerzone'),
    'page-content' => t('Content'),
  ),
);

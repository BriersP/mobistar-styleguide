<?php
/**
 * @file
 * Template for a the homepage layout.
 */
?>
<div class="tabs">
  <div id="tabs-wrapper">
    <?php print $content['tabs-wrapper'];?>
  </div>
  <div id="tabs-content-wrapper">
    <div id="tab-products" class="fp-tab-content"><?php print $content['tab-products']; ?></div>
    <div id="tab-store" class="fp-tab-content"><?php print $content['tab-store']; ?></div>
    <div id="tab-support" class="fp-tab-content"><?php print $content['tab-support']; ?></div>
    <div id="tab-customerzone" class="fp-tab-content"><?php print $content['tab-customerzone']; ?></div>
  </div>
</div>
<div id="page-content">
  <?php print $content['page-content']; ?>
</div>


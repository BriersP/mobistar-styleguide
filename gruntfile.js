module.exports = function(grunt) {

  require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      options: {
        // sourcemap: true,
        unixNewlines: true,
        lineNumbers: false,
        quiet: true
      },
      theme: {
        options: { style: 'compact' },
        files: { 'css/custom.css': 'sass/custom.scss' }
      },
      styleguide: {
        options: { style: 'extended' },
        files: { 'topdoc/styleguide.css': 'topdoc/styleguide.scss' }
      }
    },
    imagemin: {
      options: {
        pngquant: true
      },
      files: [{
        expand: true,
        cwd: 'images/',
        src: ['**/*.{png,jpg,gif}'],
        dest: 'images/'
      }]
    },
    jshint: {
      all: ['gruntfile.js']
    },
    autoprefixer: {
      options: {
        browsers: ['last 5 version', 'ie 8', 'ie 9'], // @see: https://github.com/ai/autoprefixer#browsers
        map: true
      },
      theme: {
        src: 'css/*.css'
      },
      styleguide: {
        src: 'topdoc/styleguide.css'
      }
    },
    clean: {
      theme: ["css"],
      styleguide: ["styleguide"]
    },
    topdoc: {
      local: {
        options: {
          template: 'vendor/topdoc',
          source : 'topdoc',
          destination : 'styleguide'
        }
      }
    },
    watch: {
      options: {
        livereload: true
      },
      styles: {
        files: ['sass/**/*.*'],
        tasks: ['sass:theme', 'autoprefixer:theme']
      },
      styleguide: {
        files: ['styleguide-theme/**/*.*', 'sass/**/*.*', 'topdoc/**/*.*'],
        tasks: ['clean:styleguide', 'sass:styleguide', 'autoprefixer:styleguide', 'topdoc']
      },
      scripts: {
        files: ['gruntfile.js'],
        tasks: ['jshint']
      }
    }
  });

  grunt.registerTask('default', ['sass:theme', 'autoprefixer:theme']);
  grunt.registerTask('css', ['clean:css', 'sass:theme', 'autoprefixer:theme']);
  grunt.registerTask('styleguide', ['clean:styleguide', 'sass:styleguide', 'autoprefixer:styleguide', 'topdoc']);
};

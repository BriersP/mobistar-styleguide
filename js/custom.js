(function ($) {
  Drupal.behaviors.theming = {
    attach: function (context, settings) {
      // Find the first link in this element, and navigate to its href.
      $('.clickable').click(function(div) {
          var href = $('a', this).first().attr('href');
          if (typeof href != 'undefined') {
            window.location = href;
          }
      });
      // flexslider
      $('#front-slider').flexslider({
        animation: "slide",
        animationLoop: true
      });

      var $bubbleHeight = 0,
        $bubbleWidth = 0,
        $counter = 0,
        $bubbleId = '';
      $('.bubble').each(function() {
        $counter++;
        $bubbleId = 'bubble-' + $counter;
        $(this).attr('id', $bubbleId);
        $bubbleHeight = $('#' + $bubbleId).height() + 12;
        $bubbleWidth = $('#' + $bubbleId).width() + 10;
        if($bubbleWidth > 82 ) {
          $bubbleWidth = 82;
        }
        $('head').append(
          '<style id="' + $bubbleId +'">' +
          '#' + $bubbleId +':after { ' +
          'border-right-width: ' + $bubbleWidth + 'px !important; } ' +
          '#' + $bubbleId +':before {' +
          'border-top-width: ' + $bubbleHeight + 'px !important; } ' +
          '</style>'
        );
      });
    }
  };
})(jQuery);
